import { ref, watchEffect } from "vue";
import { useRoute, useRouter } from "vue-router";

export function useBreadcrumbs() {
  const route = useRoute();
  const router = useRouter();
  const breadcrumbs = ref([]);

  // Watch for changes in the matched routes and update breadcrumbs accordingly
  watchEffect(() => {
    const matchedRoutes = route.matched || [];
    // Filter routes with breadcrumb meta and map them to text and to properties
    breadcrumbs.value = matchedRoutes
      .filter((route) => route.meta && route.meta.breadcrumb)
      .map((route) => ({ text: route.meta.breadcrumb, to: route.path }));
  });

  // Navigate back to the previous breadcrumb if available
  const navigateBack = () => {
    // Check if there are more than one breadcrumb
    if (breadcrumbs.value.length > 1) {
      // Get the previous breadcrumb
      const previousRoute = breadcrumbs.value[breadcrumbs.value.length - 2];
      // Use router to navigate to the previous route
      router.push(previousRoute.to);
    }
  };

  return {
    breadcrumbs,
    navigateBack,
  };
}
