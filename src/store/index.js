import { createStore } from "vuex";
import cart from "./cart";

const store = createStore({
  state() {
    return {
      categories: [
        { id: 1, name: "Laptops" },
        { id: 2, name: "Smartphones" },
        { id: 3, name: "Keyboards" },
        { id: 4, name: "Monitors" },
        { id: 5, name: "Chargers" },
      ],
      products: [
        {
          id: 1,
          category_id: 1,
          name: "Hp Pavilion",
          cost: 9000,
          remainder: 1,
        },
        {
          id: 2,
          category_id: 1,
          name: "Acer Aspire 3",
          cost: 7000,
          remainder: 20,
        },
        {
          id: 3,
          category_id: 1,
          name: "Macbook Pro M2",
          cost: 12000,
          remainder: 20,
        },
        {
          id: 4,
          category_id: 2,
          name: "iPhone 15",
          cost: 13000,
          remainder: 20,
        },
        {
          id: 5,
          category_id: 2,
          name: "Samsung S23",
          cost: 10000,
          remainder: 20,
        },
        {
          id: 6,
          category_id: 2,
          name: "Xiaomi 13",
          cost: 11000,
          remainder: 20,
        },
        {
          id: 7,
          category_id: 3,
          name: "Razor Pro Type Ultra",
          cost: 2000,
          remainder: 20,
        },
        {
          id: 8,
          category_id: 3,
          name: "Dell KB900",
          cost: 1600000,
          remainder: 20,
        },
        {
          id: 9,
          category_id: 3,
          name: "Cherry Stream",
          cost: 500000,
          remainder: 20,
        },
        {
          id: 10,
          category_id: 4,
          name: "HP 24mh 23.8-Inch Display",
          cost: 1800000,
          remainder: 20,
        },
        {
          id: 11,
          category_id: 4,
          name: "Dell 24 S2421HGF",
          cost: 3675000,
          remainder: 20,
        },
        {
          id: 12,
          category_id: 4,
          name: "NZXT Canvas 32Q Curved",
          cost: 4165000,
          remainder: 20,
        },
      ],
    };
  },
  getters: {
    getCategories: (state) => state.categories,
    getProducts: (state) => (filter) => {
      return state.products.filter((product) => {
        return (
          filter.category_id === "all" ||
          product.category_id === filter.category_id
        );
      });
    },
  },
  mutations: {
    removeFromStorage(state, product) {
      const found = state.products.find((el) => el.id === product.id);
      if (found) {
        found.remainder--;
      }
    },
  },
  actions: {
    removeFromStorage({ commit }, product) {
      commit("removeFromStorage", product);
    },
  },
  modules: { cart },
});

export default store;
