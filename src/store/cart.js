const cart = {
  state() {
    return {
      overallDiscount: 0,
      discount: 0,
      productCount: 0,
      products: [],
    };
  },
  mutations: {
    clearCart(state) {
      state.products = [];
      state.overallDiscount = 0;
      state.discount = 0;
      state.productCount = 0;
    },
    addDiscount(state, { target, discount }) {
      if (target === "all") {
        state.discount = discount;
      } else {
        const found = state.products.find((el) => el.id === target);
        if (found) {
          found.discount = discount;
        }
      }
    },
    removeFromCart(state, product) {
      const index = state.products.indexOf(product);
      if (index > -1) {
        state.products.splice(index, 1);
      }
    },
    addToCart(state, product) {
      const found = state.products.find((el) => el.id === product.id);
      if (found) {
        if (product.remainder > found.amount) found.amount++;
      } else {
        state.products.push({
          ...product,
          amount: 1,
          discount: 0,
        });
      }
    },
  },
  getters: {
    totalDiscount: (state) => state.discount,
    getFromCart: (state) => (productId) =>
      state.products.find((el) => el.id === productId),
    getOverallCost: (state, getters) => {
      return state.products.reduce((total, product) => {
        const discountedCost =
          product.cost - product.cost * (product.discount / 100);
        const overallDiscountedCost =
          discountedCost - discountedCost * (getters.totalDiscount / 100);
        return total + overallDiscountedCost * product.amount;
      }, 0);
    },
    getCostWithoutDis: (state, getters) => {
      return state.products.reduce((total, product) => {
        const discountedCost =
          product.cost - product.cost * (product.discount / 100);
        return total + discountedCost * product.amount;
      }, 0);
    },
    getOverallAmount: (state) =>
      state.products.reduce((total, product) => total + product.amount, 0),
    getOverallDiscount: (state, getters) => {
      // console.log(getters.getOverallCost,);
      return (
        state.products.reduce((total, product) => {
          const productDiscount =
            product.cost * (product.discount / 100) * product.amount;
          return total + productDiscount;
        }, 0) +
        (getters.totalDiscount / 100) * getters.getCostWithoutDis
      );
    },

    getAddedProducts: (state) => state.products,
  },
  actions: {
    clearCart({ commit }) {
      commit("clearCart");
    },
    addDiscount({ commit }, payload) {
      commit("addDiscount", payload);
    },
    addToCart({ commit }, product) {
      commit("addToCart", product);
    },
    removeFromCart({ commit }, product) {
      commit("removeFromCart", product);
    },
  },
};

export default cart;
