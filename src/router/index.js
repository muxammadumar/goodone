import { createRouter, createWebHistory } from "vue-router";
import Default from "../layouts/default.vue";
import Index from "../pages/index.vue";
import Payment from "../pages/payment.vue";

const routes = [
  {
    path: "/",
    component: Default,
    children: [
      {
        path: "/",
        component: Index,
        meta: { breadcrumb: 'Main' },
      },
      {
        path: "/payment",
        component: Payment,
        meta: { breadcrumb: 'Payment' },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
